package dev.tucker;

import dev.tucker.controllers.*;
import dev.tucker.util.SecurityUtil;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;


import static io.javalin.apibuilder.ApiBuilder.*;

public class Driver {

    public static void main(String[] args) {

        Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).start(80);


        BookController bookController = new BookController();
        AuthController authController = new AuthController();
        GenreController genreController = new GenreController();
        UserController userController = new UserController();
        SecurityUtil securityUtil = new SecurityUtil();



        app.routes(() -> {
            path("/admin-login", () -> {
                post(authController::authenticateAdminLogin);
                after("/", securityUtil::attachResponseHeaders);
            });
            path("/authorize-admin-token", () -> {
                get(authController::authorizeAdminToken);
                after("/", securityUtil::attachResponseHeaders);
            });
            path("/user-login", () -> {
                post(authController::authenticateUserLogin);
                after("/", securityUtil::attachResponseHeaders);
            });
            path("/register-user", () -> {
                post(authController::handlePostUserRequest);
            });
            path("/books", () -> {
               get(bookController::handleGetBooksRequest);
               path("/author", () -> {
                   path(":author", () -> {
                       get(bookController::handleGetBooksByAuthorRequest);
                   });
               });
               path("/title", () -> {
                   path(":title", () -> {
                       get(bookController::handleGetBookByTitleRequest);
                   });
               });
               path("/publisher", () -> {
                   path(":publisher", () -> {
                       get(bookController::handleGetBooksByPublisherRequest);
                   });
               });
               path("/isbn", () -> {
                   path(":isbn", () -> {
                       get(bookController::handleGetBookByIsbnNumberRequest);
                   });
               });
               path("/genre", () -> {
                   path(":title", () -> {
                       get(bookController::handleGetBooksByGenreRequest);
                   });
               });
            });
            path("/genres", () -> {
                get(genreController::handleGetGenresRequest);
            });
            path("/users", () -> {
                path("/email", () -> {
                    get(userController::getAllUserEmails);
                });
                path(":email", () -> {
                    get(userController::handleGetUserByEmail);
                    delete(userController::handleDeleteUserRequest);
                    put(userController::handlePutUpdateUserNameRequest);
                });
                path("/book-reviews", () -> {
                    post(userController::handlePostNewBookReviewRequest);
                    path(":email", () -> {
                       get(userController::handleGetBookReviewsByUserRequest);
                    });
                });
                path("/wishlist", () -> {
                   post(userController::handlePostNewBookToWishlistRequest);
                   path(":email", () -> {
                      get(userController::handleGetUserWishListByUserId);
                      put(userController::handlePutMarkHasReadRequest);
                   });
                });
            });
            path("/admin", () -> {
                path("/books", () -> {
                    before("/", authController::authorizeAdminToken);
                   post(bookController::handlePostBookRequest);
                   path(":title", () -> {
                       before("/", authController::authorizeAdminToken);
                      delete(bookController::handleDeleteBookByTitleRequest);
                   });
                   path("/description", () -> {
                       path(":title", () -> {
                           before("/", authController::authorizeAdminToken);
                           put(bookController::handleUpdateBookDescription);
                       });
                   });
                   path("/publisher", () -> {
                       path(":title", () -> {
                           before("/", authController::authorizeAdminToken);
                           put(bookController::handleUpdateBookPublisher);
                       });
                   });
                   path("/genres", () -> {
                      path(":title", () -> {
                          before("/", authController::authorizeAdminToken);
                          put(bookController::handleUpdateBookGenreList);
                      });
                   });

                });
                path("/genre", () -> {
                    before("/", authController::authorizeAdminToken);
                   post(genreController::handlePostGenreRequest);
                   path(":genre-title", () -> {
                       before("/", authController::authorizeAdminToken);
                      delete(genreController::handleDeleteGenreRequest);
                      put(genreController::handlePutNewGenreDescriptionRequest);
                   });
                });
                path("/user", () -> {
                    before("/", authController::authorizeAdminToken);
                    get(userController::handleGetUserRequest);
                });
                path("/author", () -> {
                    before("/", authController::authorizeAdminToken);
                    post(bookController::handlePostAuthor);
                });
            });
        });



    }
}
