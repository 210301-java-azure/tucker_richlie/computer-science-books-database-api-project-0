# **Computer Science Books Database API**

This API can be broadly divided into three areas of functionality based on user permissions:
&nbsp;&nbsp;&nbsp;  -General Users (no login required)
&nbsp;&nbsp;&nbsp;    -Functionality for registered users
&nbsp;&nbsp;&nbsp;    -Functionality reserved for site administrators

## General Usage
-------------------------------------------------------------------------------------
The following RESTful CRUD methods allows the user to browse the selection of books stored in the database and search for various books based on the criteria defined below:

Register
> POST HTTP http://localhost:7000/register-user

Using the JSON format for the request body:

```
{
        "name": "John Doe",
        "password": "temporaryPass",
        "email": "jdoe@gmail.com"
}
```

Get all books in database:
>HTTP GET http://localhost:7000/books

Get books by author:
> HTTP GET http://localhost:7000/books/author/{author}
> Example: HTTP GET http://localhost:7000/books/author/Brian%20W.%20Kernighan

Get books by title:
> HTTP GET http://localhost:7000/books/title/{title}
> Example: HTTP GET http://localhost:7000/books/title/The%20C%20Programming%20Language

Get books by publisher:
> HTTP GET http://localhost:7000/books/publisher/{publisher}
Example: HTTP GET http://localhost:7000/books/publisher/MIT%20Press

Get books by ISBN:
> HTTP GET http://localhost:7000/books/isbn/{isbn}
> Example: HTTP GET http://localhost:7000/books/isbn/0262510871

Get books by genre:
> HTTP GET http://localhost:7000/books/genre/{genre}
> Example: HTTP GET http://localhost:7000/books/genre/programming%20languages

Get all genres with their descriptions:
> HTTP GET http://localhost:7000/genres



## Features for Registered Users
-------------------------------------------------------------------------------------
In order to use these features you must have valid login credentials, i.e., an email and password. 
Register as a user using the following POST Request:
> HTTP POST http://localhost:7000/register-user

Using the following JSON format:

```
{
    "name": "John Doe",
    "password": "myPassword",
    "email" : "jd@mail.com"
}
```

Set Form Parameters in PostMan to email=yourEmail and password=yourPassword and login as follows:
> HTTP GET http://localhost:7000/user-login

You should now be able to rate books and add books to your wishlist.
To add a new book review use
> HTTP POST http://localhost:7000/users/book-reviews

and make sure you have valid JSON in the request body of the following form:
```
{
        "book": "Concrete Mathematics: A Foundation for Computer Science",
        "rating": 4.5,
        "email": "jd@mail.com"
}
```

Delete your user account:
> DELETE HTTP http://localhost:7000/user/{email}
> Example: DELETE HTTP http://localhost:7000/user/jd@mail.com

Change your name:
> PUT HTTP http://localhost:7000/user/{email}
> Example: PUT HTTP http://localhost:7000/user/jd@mail.com

Using the JSON format for the request body:

``` 
{
    "name": "John Doe"
}
```

Get a list of all the books you've reviewed:

> GET HTTP http://localhost:7000/users/book-reviews/{email}
> Example: GET HTTP http://localhost:7000/users/book-reviews/jd@mail.com

Add a new book to your wishlist:

> POST HTTP http://localhost:7000/users/wishlist

Use the following JSON format in the HTTP request body:

```
{
        "book": "Effective Java",
        "hasRead": false, //Can set this to true later once done with book
        "email": "jd@mail.com"
}
```

Get a list of all books in your wishlist:

> GET HTTP http://localhost:7000/users/wishlist/{email}
> Example: GET HTTP http://localhost:7000/users/wishlist/jd@mail.com

Change the value of hasRead to true or false for a book in your wishlist (use the same format for the request body as above):

> PUT HTTP http://localhost:7000/users/wishlist/{email}
> Example: PUT HTTP http://localhost:7000/users/wishlist/jd@mail.com

## Admin Features
------------------------------------------------------------------------------------
The following functionality can only be accessed if you're logged in as admin.

Set Form Parameters in PostMan to username=yourAdminUsername and password=adminPassword and login as follows: 
> GET HTTP http://localhost:7000/admin-login

Add a new book to database:
> POST HTTP http://localhost:7000/admin/books

Use the following JSON format in the request body:
```
{
        "title": "Computer Networks",
        "publisher": "Prentice Hall",
        "date": "08/09/2002",
        "isbn": "0130661023",
        "description": "For courses in computer networking or introductions to networking at both the undergraduate and graduate level in computer science, engineering, CIS, MIS, and business departments. In this revision, the author takes a structured approach to explaining how networks function.",
        "rating": 0,
        "genres": [
            "networking"
        ],
        "authors": [
            "Andrew S. Tanenbaum"
        ]   
}
```

Delete a book from the database by ISBN:
> DELETE HTTP http://localhost:7000/admin/books/{isbn}
> Example: DELETE HTTP http://localhost:7000/admin/books/0130661023

Update a book's description by ISBN:
> PUT HTTP http://localhost:7000/admin/books/description/{isbn}
> Example: PUT HTTP http://localhost:7000/admin/books/description/0130661023

Using the JSON format for the request body:
```
{
    "description" : "Concrete Mathematics is a blending of continuous and discrete mathematics. 'More concretely,' the authors explain, 'it is the controlled manipulation of mathematical formulas, using a collection of techniques for solving problems'"
}
```
Update a book's publisher by ISBN:
> PUT HTTP http://localhost:7000/admin/books/publisher/{isbn}
> Example PUT HTTP http://localhost:7000/admin/books/publisher/0130661023

Using the JSON format for the request body:

```
{
    "publisher": "MIT Press"
}
```

Add a new genre category to a book by title:
> PUT HTTP http://localhost:7000/admin/books/genres/{title}
> Example: PUT HTTP http://localhost:7000/admin/books/genres/Concrete%20Mathematics:%20A%20Foundation%20for%20Computer%20Science

Using the JSON format for the request body:

```
{
    "genre" : "theory"
}
```

Add a new genre to the list of genres:
> POST HTTP http://localhost:7000/admin/genre

Using the JSON format for the request body:

```
{
        "genreTitle": "compilers",
        "description": "In computing, a compiler is a computer program that translates computer code written in one programming language into another language."
}
```

Delete a genre:
> DELETE HTTP http://localhost:7000/admin/genre/{genreTitle}
> Example: DELETE HTTP http://localhost:7000/admin/genre/compilers

Change description of an already existing genre:
> PUT HTTP http://localhost:7000/admin/genre/{genreTitle}
> Example: PUT HTTP http://localhost:7000/admin/genre/machine%20learning

Using the JSON format for the request body:

```
{
    "description": "Machine learning is the study of computer algorithms that improve automatically through experience and by the use of data. It is seen as a part of artificial intelligence."
}
```

Get a list of all users:
> GET HTTP http://localhost:7000/admin/user















    
 










