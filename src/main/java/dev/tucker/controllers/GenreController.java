package dev.tucker.controllers;

import dev.tucker.models.Genre;
import dev.tucker.services.GenreService;
import io.javalin.http.Context;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenreController {

    Logger logger = LoggerFactory.getLogger(GenreController.class);
    GenreService genreService = new GenreService();

    public void handleGetGenresRequest(Context ctx) {
        logger.info("Retrieving all genres with their descriptions.");
        ctx.json(genreService.getAll());
    }

    public void handlePostGenreRequest(Context ctx) {
        logger.info("Adding a genre to the database.");
        Genre genre = ctx.bodyAsClass(Genre.class);
        genreService.addGenre(genre);
        ctx.status(201);
    }

    public void handleDeleteGenreRequest(Context ctx) {
        logger.info("Deleting a genre from the database");
        String genreTitle = ctx.pathParam("genre-title");
        genreService.delete(genreTitle);
        ctx.status(200);
    }

    public void handlePutNewGenreDescriptionRequest(Context ctx) {
        String genreTitle = ctx.pathParam("genre-title");
        logger.info("Updating description of the " + genreTitle + " genre.");
        String json = ctx.body();
        JSONObject obj = new JSONObject(json);
        String newDescription = obj.getString("description");
        Genre newGenreObject = new Genre(genreTitle, newDescription);
        genreService.update(newGenreObject);
        logger.info("Done");
        ctx.status(200);
    }
}
