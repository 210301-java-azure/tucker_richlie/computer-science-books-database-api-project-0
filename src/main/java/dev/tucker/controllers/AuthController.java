package dev.tucker.controllers;

import dev.tucker.models.Member;
import dev.tucker.services.AuthServices;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthController{

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);

    public void authenticateAdminLogin(Context ctx) {
        String user = ctx.formParam("username");
        String password = ctx.formParam("password");
        logger.info("Attempting to login");
        AuthServices authServices = new AuthServices();

        if (user != null && password != null && authServices.checkAdminCredentials(user, password) != 0){
            logger.info("Login successful");
            ctx.header("Authorization", "admin-auth-token");
            ctx.status(200);
        }
        else {
            throw new UnauthorizedResponse("Either user name or password is incorrect");
        }
    }

    public void authenticateUserLogin(Context ctx) {
        String userEmail = ctx.formParam("email");
        String password = ctx.formParam("password");
        logger.info("Attempting to login");
        AuthServices authServices = new AuthServices();
        if (userEmail != null && password != null && authServices.checkUserCredentials(userEmail, password) != 0) {
                logger.info("Login Successful");
                ctx.header("Authorization", "user-auth-token-" + userEmail);
                ctx.status(200);
        }
        else {
                throw new UnauthorizedResponse("Either email or password is incorrect");
        }
        }



    public void authorizeAdminToken(Context ctx) {

        if(ctx.method().equals("OPTIONS")){
            return;
        }

        String token = ctx.header("Authorization");
        if (token != null && token.equals("admin-auth-token")) {
            logger.info("Request is authorized.");
            ctx.status(200);
        }
        else  {
            logger.info("Request is not authorized.");
            throw new UnauthorizedResponse();
        }
    }

    public void handlePostUserRequest(Context ctx) {
        logger.info("Adding a user to the database.");
        Member user = ctx.bodyAsClass(Member.class);
        AuthServices authServices = new AuthServices();
        authServices.add(user);
        ctx.status(201);
    }

    public void handleOptionsRequest(Context ctx) {
        if (ctx.method().equals("OPTIONS")) {
            return;
        }
    }

}
