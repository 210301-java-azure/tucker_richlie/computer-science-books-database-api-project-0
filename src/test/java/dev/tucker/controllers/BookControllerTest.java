package dev.tucker.controllers;


import dev.tucker.models.Book;
import dev.tucker.models.Genre;
import dev.tucker.services.BookService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class BookControllerTest {



    /*   @InjectMocks
    private BookController bookController = new BookController();


    @Mock
    private BookService bookService = new BookService();

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testHandleGetBooksRequest() {
        List<Book> books = new ArrayList<>();
        List<String> genres1 = new ArrayList<>();
        List<String> genres2 = new ArrayList<>();
        List<String> authors1 = new ArrayList<>();
        List<String> authors2 = new ArrayList<>();
        genres1.add("programming languages");
        genres1.add("software engineering");
        authors1.add("Brian W. Kernighan");
        authors1.add("Dennis M. Ritchie");
        genres2.add("programming languages");
        authors2.add("Alan A.A. Donovan");
        authors2.add("Brian W. Kernighan");
        Context context = mock(Context.class);
        books.add(new Book("The C Programming Language", "MIT Press", "03/22/1988", "0131103628", "This book is meant to help the reader learn how to program in C. It is the definitive reference guide, now in a second edition. Although the first edition was written in 1978, it continues to be a worldwide best-seller. This second edition brings the classic original up to date to include the ANSI standard. The C Programming Language by Kernighan and Ritchie is a computer science classic.", 4.5, genres1, authors1));
        books.add(new Book("The Go Programming Language", "Addison-Wesley", "08/27/2015","0134190440", "Go is an open-source programming language that makes it easy to build clean, reliable, and efficient software. It has been winning converts from dynamic language enthusiasts as well as users of traditional compiled languages. The former appreciate the robustness and efficiency that Gos lightweight type system brings to their code; the latter find Gos simplicity and fast tools a refreshing change. Thanks to its well-designed standard libraries and its excellent support for concurrent programming, Go is fast becoming the language of choice for distributed systems.", 4.4, genres2, authors2));
        when(bookService.getAll()).thenReturn(books);
        bookController.handleGetBooksRequest(context);
        verify(context).json(books);
    }

    @Test
    public void testHandlePostBookRequest() {
        Context context = mock(Context.class);
        bookController.handlePostBookRequest(context);
        verify(context).status(201);
    } */
}

