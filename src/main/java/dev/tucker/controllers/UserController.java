package dev.tucker.controllers;

import dev.tucker.models.Book;
import dev.tucker.models.BookReview;
import dev.tucker.models.Member;
import dev.tucker.models.WishlistItem;
import dev.tucker.services.UserService;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.http.UnauthorizedResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class UserController {

    Logger logger = LoggerFactory.getLogger(UserController.class);
    UserService userService = new UserService();
    AuthController authController = new AuthController();

    public void handleGetUserRequest(Context ctx) {
        logger.info("Getting list of users");
        ctx.json(userService.getAll());
    }

    public void handleGetUserByEmail(Context ctx) {
        authController.handleOptionsRequest(ctx);
        String email = ctx.pathParam("email");
        String token = ctx.header("Authorization");
        logger.info("token:" + token);
        if (token != null && token.equals("user-auth-token-" + email)) {
            logger.info("Request is authorized.");
            Member user = userService.getUserByEmail(email);
            ctx.json(user);
        }
        else  {
            logger.info("Request is not authorized.");
            throw new UnauthorizedResponse();
        }
    }

    public void handleGetBookReviewsByUserRequest(Context ctx) {
        authController.handleOptionsRequest(ctx);
        String email = ctx.pathParam("email");
        String token = ctx.header("Authorization");
        logger.info("token:" + token);
        if (token != null && token.equals("user-auth-token-" + email)) {
            logger.info("Request is authorized.");
            List<BookReview> bookReviews = userService.getBookReviewsForUser(email);
            if (bookReviews.isEmpty()) {
                logger.warn("user has not reviewed any books");
                throw new NotFoundResponse("You have not reviewed any books");
            }
            else {
                logger.info("retrieving book reviews of user " + email);
                ctx.json(bookReviews);
            }
        }
        else  {
            logger.info("Request is not authorized.");
            throw new UnauthorizedResponse();
        }

    }

    public void handleGetUserWishListByUserId(Context ctx) {
        authController.handleOptionsRequest(ctx);
        String email = ctx.pathParam("email");
        String token = ctx.header("Authorization");
        if (token != null && token.equals("user-auth-token-" + email)) {
            logger.info("Request is authorized.");
            List<WishlistItem> wishlistItems = userService.getWishlistForUser(email);
            if (wishlistItems.isEmpty()) {
                logger.warn("user has no books in wishlist");
                throw new NotFoundResponse("Your wishlist is empty");
            }
            else {
                logger.info("retrieving wishlist of user " + email);
                    ctx.json(wishlistItems);
                }
        }

        else  {
            logger.info("Request is not authorized.");
            throw new UnauthorizedResponse();
        }

    }

    public void handlePostNewBookReviewRequest(Context ctx) {
        authController.handleOptionsRequest(ctx);
        BookReview bookReview = ctx.bodyAsClass(BookReview.class);
        String email = bookReview.getUser().getEmail();
        String token = ctx.header("Authorization");
        logger.info("token:" + token);
        logger.info("user-auth-token-" + email);
        if (token != null && token.equals("user-auth-token-" + email)) {
            logger.info("Request is authorized.");
            logger.info("submitting request");
            userService.addNewBookReview(bookReview);
            logger.info("Request finished");
            ctx.status(201);
            logger.info("User " + bookReview.getUser().getEmail() + " reviewed a new book");
        }
        else  {
            logger.info("Request is not authorized.");
            throw new UnauthorizedResponse();
        }

    }

    public void handlePostNewBookToWishlistRequest(Context ctx) {
        authController.handleOptionsRequest(ctx);
        WishlistItem wishlistItem = ctx.bodyAsClass(WishlistItem.class);
        String email = wishlistItem.getUser().getEmail();
        String token = ctx.header("Authorization");
        if (token != null && token.equals("user-auth-token-" + email)) {
            logger.info("Request is authorized.");
            userService.addNewBookToWishlist(wishlistItem);
            ctx.status(201);
            logger.info("User " + wishlistItem.getUser().getEmail() + " added a new book to their wishlist");
        }
        else  {
            logger.info("Request is not authorized.");
            throw new UnauthorizedResponse();
        }
    }

    public void handleDeleteUserRequest(Context ctx) {
        authController.handleOptionsRequest(ctx);
        logger.info("Deleting user from database");
        String email = ctx.pathParam("email");
        String token = ctx.header("Authorization");
        if (token != null && token.equals("user-auth-token-" + email)) {
            logger.info("requestIsAuthorized");
            userService.delete(email);
            ctx.status(200);
        }

        else {
            logger.info("Request is not authorized.");
            throw new UnauthorizedResponse();
        }


    }

    public void handlePutMarkHasReadRequest(Context ctx) {
        authController.handleOptionsRequest(ctx);
        String email = ctx.pathParam("email");
        String token = ctx.header("Authorization");
        if (token != null && token.equals("user-auth-token-" + email)) {
            logger.info("Request is authorized.");
            String json = ctx.body();
            JSONObject obj = new JSONObject(json);
            String title = obj.getString("book");
            boolean newHasRead = obj.getBoolean("hasRead");
            Book book = new Book(title);
            Member user = new Member(email);
            WishlistItem wishlistItem = new WishlistItem(book, newHasRead, user);
            userService.updateHasRead(wishlistItem);
            logger.info("hasRead field updated");
            ctx.status(200);

        }

        else  {
            logger.info("Request is not authorized.");
            throw new UnauthorizedResponse();
        }
    }

    public void handlePutUpdateUserNameRequest(Context ctx) {
        authController.handleOptionsRequest(ctx);
        logger.info("Updating specified fields");
        String email = ctx.pathParam("email");
        String token = ctx.header("Authorization");
        if (token != null && token.equals("user-auth-token-" + email)) {
            logger.info("Request is authorized");
            String json = ctx.body();
            JSONObject obj = new JSONObject(json);
            String newUserName = obj.getString("name");
            Member newUserObject = new Member(newUserName, email);
            userService.update(newUserObject);
            ctx.status(200);
        }

        else {
            logger.info("Request is not authorized.");
            throw new UnauthorizedResponse();
        }


    }


    public void getAllUserEmails(Context ctx) {
        ctx.json(userService.getAllUserEmails());
    }

}
