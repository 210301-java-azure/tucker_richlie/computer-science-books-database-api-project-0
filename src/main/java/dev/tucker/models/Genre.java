package dev.tucker.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Genre implements Serializable {

    @Id
    private String genreTitle;
    @Lob
    private String description;


    public Genre() {
        super();
    }

    public Genre(String genreTitle, String description) {
        this.genreTitle = genreTitle;
        this.description = description;
    }

    public Genre(String genreTitle) {
        this.genreTitle = genreTitle;
    }

    public String getGenreTitle() {
        return genreTitle;
    }

    public void setGenreTitle(String genreTitle) {
        this.genreTitle = genreTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return Objects.equals(genreTitle, genre.genreTitle) && Objects.equals(description, genre.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(genreTitle, description);
    }

    @Override
    public String toString() {
        return "Genre{" +
                "genreTitle='" + genreTitle + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}