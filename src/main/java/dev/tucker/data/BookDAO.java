package dev.tucker.data;

import dev.tucker.models.Author;
import dev.tucker.models.Book;
import dev.tucker.models.Genre;


import java.util.List;


public interface BookDAO {

    public List<Book> getAllBooks();
    public Book getBookInfoByTitle(String title);
    public Book getBookInfoByIsbn(String isbn);
    public List<Book> getBooksByPublisher(String publisher);
    public List<Book> getBooksByGenre(Genre genre);
    public List<Book> getBooksByAuthor(String author);
    public void addNewBook(Book book);
    public void deleteBook(String title);


    public void updateBookDescription(Book book);
    public void updateBookPublisher(Book book);
    public void updateBookGenres(Book book);
    public void addAuthor(Author author);
}
