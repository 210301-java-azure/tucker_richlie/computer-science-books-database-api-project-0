package dev.tucker.services;

import dev.tucker.data.BookDAO;
import dev.tucker.hibdata.BookDAOHibImp;
import dev.tucker.models.Author;
import dev.tucker.models.Book;
import dev.tucker.models.Genre;

import java.util.List;


public class BookService {

    BookDAO bookDAOImp = new BookDAOHibImp();

    public List<Book> getAll() {
        return bookDAOImp.getAllBooks();
    }

    public Book getBookByTitle(String title) {
        return bookDAOImp.getBookInfoByTitle(title);
    }

    public Book getBookByIsbn(String isbn) {
        return bookDAOImp.getBookInfoByIsbn(isbn);
    }

    public List<Book> getBooksByPublisher(String publisher) {
        return bookDAOImp.getBooksByPublisher(publisher);
    }

    public List<Book> getBooksByGenre(Genre genre) {
        return bookDAOImp.getBooksByGenre(genre);
    }

    public List<Book> getBooksByAuthor(String author) {
        return bookDAOImp.getBooksByAuthor(author);
    }

    public void add(Book book) {
        bookDAOImp.addNewBook(book);

    }

    public void delete(String isbn) {
        bookDAOImp.deleteBook(isbn);

    }

    public void updateDescription(Book book) {
        bookDAOImp.updateBookDescription(book);
    }

    public void updatePublisher(Book book) {
        bookDAOImp.updateBookPublisher(book);
    }

    public void updateBookGenreList(Book book) {
        bookDAOImp.updateBookGenres(book);
    }

    public double getAverageRating(List<Double> ratings) {
        double sum = 0;
        for (int i = 0; i < ratings.size(); i++) {
            sum += ratings.get(i);
        }
        double average = sum/(ratings.size());
        return average;
    }

    public void addAuthor(Author author) {
        bookDAOImp.addAuthor(author);
    }

}
