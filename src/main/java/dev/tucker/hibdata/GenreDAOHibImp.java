package dev.tucker.hibdata;

import dev.tucker.data.GenreDAO;
import dev.tucker.models.Genre;
import dev.tucker.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class GenreDAOHibImp implements GenreDAO {

    Logger logger = LoggerFactory.getLogger(BookDAOHibImp.class);

    @Override
    public List<Genre> getAllGenreDescriptions() {
        try(Session s = HibernateUtil.getSession()) {
           List<Genre> genreList =  s.createQuery("from Genre", Genre.class).list();
           return genreList;
        }
    }

    @Override
    public void addNewGenre(Genre genre) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.persist(genre);
            tx.commit();
        }

    }

    @Override
    public void deleteGenre(String genreName) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.delete(new Genre(genreName));
            tx.commit();
        }
    }

    @Override
    public void updateGenreDescription(Genre genre) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.update(genre);
            tx.commit();
        }
    }
}
