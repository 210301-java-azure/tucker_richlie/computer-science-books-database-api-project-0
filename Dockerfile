FROM java:8
COPY build/libs/book-server-1.0-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar book-server-1.0-SNAPSHOT.jar