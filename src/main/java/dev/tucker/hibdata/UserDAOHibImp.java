package dev.tucker.hibdata;

import dev.tucker.data.UserDAO;
import dev.tucker.models.*;
import dev.tucker.services.BookService;
import dev.tucker.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class UserDAOHibImp implements UserDAO {

    Logger logger = LoggerFactory.getLogger(BookDAOHibImp.class);

    @Override
    public List<Member> getAllUsers() {
        try(Session s = HibernateUtil.getSession()) {
            List<Member> userList =  s.createQuery("from Member", Member.class).list();
            return userList;
        }
    }

    @Override
    public Member getUser(String email) {
        try (Session s = HibernateUtil.getSession()) {
            Member user = s.get(Member.class, email);
            logger.info("got User with email: " + email);
            return user;
        }
    }


    // Add dynamic updating of books rating field
    @Override
    public List<BookReview> getBookReviewsByUserId(String email) {
        try (Session s = HibernateUtil.getSession()) {
            Query<BookReview> q = s.createQuery("from BookReview where email=:email", BookReview.class);
            q.setParameter("email", email);
            List<BookReview> bookReview = q.list();
            return bookReview;
        }

    }

    @Override
    public List<WishlistItem> getWishListByUserId(String email) {
        try (Session s = HibernateUtil.getSession()) {
            Query<WishlistItem> q = s.createQuery("from WishlistItem where email=:email", WishlistItem.class);
            q.setParameter("email", email);
            List<WishlistItem> wishlistItems = q.list();
            return wishlistItems;
        }
    }

    @Override
    public void addNewBookReview(BookReview bookReview) {
        try (Session s = HibernateUtil.getSession()) {
            BookService bookService = new BookService();
            Transaction tx = s.beginTransaction();
            s.persist(bookReview);
            Query<Double> query = s.createQuery("select rating from BookReview where bookTitle = :bookTitle");
            query.setParameter("bookTitle", bookReview.getBook());
            List<Double> ratings = query.list();
            double average = bookService.getAverageRating(ratings);
            Query q = s.createQuery("update Book b set b.rating = :rating where b.title = :title");
            q.setParameter("rating", average);
            q.setParameter("title", bookReview.getBook().getTitle());
            q.executeUpdate();
            tx.commit();
        }
    }

    @Override
    public void addNewBookToWishList(WishlistItem wishlistItem) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.persist(wishlistItem);
            tx.commit();
        }
    }

    @Override
    public void deleteUser(String email) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            Member user = new Member(email);
            s.delete(user);
            tx.commit();
        }
    }

    @Override
    public void updateUserName(Member user) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            Query q = s.createQuery("update Member m set m.name=:name where m.email=:email");
            q.setParameter("name", user.getName());
            q.setParameter("email", user.getEmail());
            q.executeUpdate();
            tx.commit();
        }
    }

    @Override
    public void updateHasRead(WishlistItem wishlistItem) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            Query q = s.createQuery("update WishlistItem w set w.hasRead=:hasRead where w.user=:user");
            q.setParameter("hasRead", wishlistItem.isHasRead());
            q.setParameter("user", wishlistItem.getUser());
            q.executeUpdate();
            tx.commit();
        }
    }

    @Override
    public List<String> getUserEmails() {
        try(Session s = HibernateUtil.getSession()) {
            Query<String> q = s.createQuery("select email from Member");
            List<String> userList = q.list();
            return userList;
        }
    }
}
