package dev.tucker.util;

import dev.tucker.models.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    private static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            Configuration configuration = new Configuration();
            Properties settings = new Properties();
            settings.put(Environment.URL, System.getenv("DB_URL"));
            settings.put(Environment.USER, System.getenv("DB_USER"));
            settings.put(Environment.PASS, System.getenv("DB_PASS"));

            settings.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            settings.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");

            settings.put(Environment.HBM2DDL_AUTO, "validate");
            settings.put(Environment.SHOW_SQL, "true");

            configuration.setProperties(settings);

            //Provide hibernate mappings to configuration
            configuration.addAnnotatedClass(Genre.class);
            configuration.addAnnotatedClass(Member.class);
            configuration.addAnnotatedClass(Admin.class);
            configuration.addAnnotatedClass(Author.class);
            configuration.addAnnotatedClass(Book.class);
            configuration.addAnnotatedClass(BookReview.class);
            configuration.addAnnotatedClass(WishlistItem.class);


            sessionFactory = configuration.buildSessionFactory();


        }
        return sessionFactory;

    }

    public static Session getSession() {
        return getSessionFactory().openSession();
    }
}
