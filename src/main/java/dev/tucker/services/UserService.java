package dev.tucker.services;

import dev.tucker.data.UserDAO;
import dev.tucker.hibdata.UserDAOHibImp;
import dev.tucker.models.BookReview;
import dev.tucker.models.Member;
import dev.tucker.models.WishlistItem;

import java.util.List;

public class UserService {

    UserDAO userDAOImp = new UserDAOHibImp();

    public List<Member> getAll() {
        return userDAOImp.getAllUsers();
    }

    public Member getUserByEmail(String email) {
        return userDAOImp.getUser(email);
    }

    public List<BookReview> getBookReviewsForUser(String email) {
        return userDAOImp.getBookReviewsByUserId(email);
    }

    public List<WishlistItem> getWishlistForUser(String email) {
        return userDAOImp.getWishListByUserId(email);
    }

    public void addNewBookReview(BookReview bookReview) {
        userDAOImp.addNewBookReview(bookReview);
    }

    public void addNewBookToWishlist(WishlistItem wishlistItem) {
        userDAOImp.addNewBookToWishList(wishlistItem);
    }

    public void delete(String email) {
        userDAOImp.deleteUser(email);
    }

    public void update(Member user) {
        userDAOImp.updateUserName(user);
    }

    public void updateHasRead(WishlistItem wishlistItem) {
        userDAOImp.updateHasRead(wishlistItem);
    }

    public List<String> getAllUserEmails(){
        return userDAOImp.getUserEmails();
    }
}
