package dev.tucker.services;

import dev.tucker.data.GenreDAO;
import dev.tucker.hibdata.GenreDAOHibImp;
import dev.tucker.models.Genre;

import java.util.List;

public class GenreService {

    GenreDAO genreDAOImp = new GenreDAOHibImp();


    public List<Genre> getAll() {
        return genreDAOImp.getAllGenreDescriptions();
    }

    public void addGenre(Genre genre) {
        genreDAOImp.addNewGenre(genre);
    }

    public void delete(String genreTitle) {
        genreDAOImp.deleteGenre(genreTitle);

    }

    public void update(Genre genre) {
        genreDAOImp.updateGenreDescription(genre);
    }
}
