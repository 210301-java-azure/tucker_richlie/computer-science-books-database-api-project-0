package dev.tucker.data;

import dev.tucker.models.Member;

public interface AuthDAO {

    public int checkAdminCredentials(String userName, String password);
    public int checkUserCredentials(String email, String password);
    public void addNewUser(Member user);
}
