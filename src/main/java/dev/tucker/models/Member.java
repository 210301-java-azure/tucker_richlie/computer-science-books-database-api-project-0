package dev.tucker.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Member implements Serializable {


    private String name;
    private String password;
    @Id
    private String email;

    public Member() {
        super();
    }

    public Member(String email) {
        this.email= email;
    }

    public Member(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public Member(String name, String password, String email) {
        this.name = name;
        this.password = password;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   /* public List<BookReview> getUserBookReviews() {
        return userBookReviews;
    }

    public void addBookReview(BookReview bookReview) {
        userBookReviews.add(bookReview);
    }

    public List<WishlistItem> getUserWishlist() {
        return userWishlist;
    }

    public void addWishlistItem(WishlistItem userWishListItem) {
        userWishlist.add(userWishListItem);
    } */

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member user = (Member) o;
        return Objects.equals(name, user.name) && Objects.equals(password, user.password) && Objects.equals(email, user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, password, email);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
