package dev.tucker.data;


import dev.tucker.models.Genre;

import java.util.List;

public interface GenreDAO {

    public List<Genre> getAllGenreDescriptions();
    public void addNewGenre(Genre genre);
    public void deleteGenre(String genreName);
    public void updateGenreDescription(Genre genre);
}
