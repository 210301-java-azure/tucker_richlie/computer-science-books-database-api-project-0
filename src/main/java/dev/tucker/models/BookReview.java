package dev.tucker.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class BookReview implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name="title")
    private Book bookTitle;


    private double rating;

    @Id
    @ManyToOne
    @JoinColumn(name="email")
    private Member user;

    public BookReview() {
        super();
    }

    public BookReview(Book bookTitle, double rating) {
        this.bookTitle = bookTitle;
        this.rating = rating;
    }

    public BookReview(Book bookTitle, double rating, Member user) {
        this.bookTitle = bookTitle;
        this.rating = rating;
        this.user = user;
    }

    public Book getBook() {
        return bookTitle;
    }

    public void setBook(Book bookTitle) {
        this.bookTitle = bookTitle;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public Member getUser() {
        return user;
    }

    public void setUser(Member user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookReview that = (BookReview) o;
        return Double.compare(that.rating, rating) == 0 && Objects.equals(bookTitle, that.bookTitle) && Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookTitle, rating, user);
    }

    @Override
    public String toString() {
        return "BookReview{" +
                "bookTitle=" + bookTitle +
                ", rating=" + rating +
                ", user=" + user +
                '}';
    }
}
