package dev.tucker.controllers;

import dev.tucker.models.Author;
import dev.tucker.models.Book;
import dev.tucker.models.Genre;
import dev.tucker.services.BookService;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class BookController {

    private Logger logger = LoggerFactory.getLogger(BookController.class);
    private BookService bookService = new BookService();

    public void handleGetBooksRequest(Context ctx) {
        logger.info("Retrieving all book listings.");
        ctx.json(bookService.getAll());
    }

    public void handleGetBookByTitleRequest(Context ctx) {
        String title = ctx.pathParam("title");
        Book book = bookService.getBookByTitle(title);
        if (book == null) {
            logger.warn("There is no book with title " + title);
            throw new NotFoundResponse(title + " does not exist in database.");
        }
        else {
            logger.info("Retrieving info for book " + title);
            ctx.json(book);
        }
    }

    public void handleGetBookByIsbnNumberRequest(Context ctx) {
        String isbn = ctx.pathParam("isbn");
        Book book = bookService.getBookByIsbn(isbn);
        if (book == null) {
            logger.warn("There is no book with isbn number: " + isbn);
            throw new NotFoundResponse("Book with isbn number " + isbn + " does not exist in database");
        }
        else {
            logger.info("Retrieving info for book " + isbn);
            ctx.json(book);
        }
    }

    public void handleGetBooksByPublisherRequest(Context ctx) {
        String publisher = ctx.pathParam("publisher");
        List<Book> books = bookService.getBooksByPublisher(publisher);
        if (books.isEmpty()) {
            logger.warn("There are no books in the database published by " + publisher);
            throw new NotFoundResponse("Publisher " + publisher + " does not exist in database");
        }
        else {
            logger.info("Retrieving book(s) with publisher " + publisher);
            ctx.json(books);
        }

    }

    public void handleGetBooksByGenreRequest(Context ctx) {
        String genreTitle = ctx.pathParam("title");
        Genre genre = new Genre(genreTitle);
        List<Book> books = bookService.getBooksByGenre(genre);
        if (books.isEmpty()) {
            logger.warn("There are no books in the database in genre " + genreTitle);
            throw new NotFoundResponse("There are no books in the " + genreTitle + " genre in the database");
        }
        else {
            logger.info("Retrieving book(s) of the " + genreTitle + " genre");
            ctx.json(books);
        }
    }

    public void handleGetBooksByAuthorRequest(Context ctx) {
        String author = ctx.pathParam("author");
        List<Book> books = bookService.getBooksByAuthor(author);
        if (books.isEmpty()) {
            logger.warn("There are no books in the database with author " + author);
            throw new NotFoundResponse("There are no books with " + author + " as author in the database");
        }
        else {
            logger.info("Retrieving books(s) with author " + author);
            ctx.json(books);
        }


    }

    public void handlePostBookRequest(Context ctx) {
        Book book = ctx.bodyAsClass(Book.class);
        bookService.add(book);
        ctx.status(201);
        logger.info("Adding a book to the database.");
    }

    public void handleDeleteBookByTitleRequest(Context ctx) {
        logger.info("Deleting book from database");
        String title = ctx.pathParam("title");
        bookService.delete(title);
        ctx.status(200);
    }

    public void handleUpdateBookDescription(Context ctx) {
        String title = ctx.pathParam("title");
        logger.info("Updating description of book" + title);
        String json = ctx.body();
        JSONObject obj = new JSONObject(json);
        String newDescription = obj.getString("description");
        Book book = new Book(title, newDescription, null);
        logger.info("book: " + book);
        bookService.updateDescription(book);
        ctx.status(200);

    }

    public void handleUpdateBookPublisher(Context ctx) {
        String title = ctx.pathParam("title");
        logger.info("Updating publisher of book" + title);
        String json = ctx.body();
        JSONObject obj = new JSONObject(json);
        String newPublisher = obj.getString("publisher");
        logger.info("publisher: " + newPublisher);
        Book book = new Book(title, null, newPublisher);
        logger.info("book: " + book);
        bookService.updatePublisher(book);
        ctx.status(200);
    }


    public void handleUpdateBookGenreList(Context ctx) {
        String title = ctx.pathParam("title");
        logger.info("Associating genre with a book.");
        String json = ctx.body();
        JSONObject obj = new JSONObject(json);
        String newGenre = obj.getString("genre");
        Genre genre = new Genre(newGenre);
        Book book = new Book(title);
        book.addGenres(genre);
        logger.info("Book: " + book);
        bookService.updateBookGenreList(book);
        ctx.status(200);
    }

    public void handlePostAuthor(Context ctx) {
        Author author = ctx.bodyAsClass(Author.class);
        bookService.addAuthor(author);
        ctx.status(201);
        logger.info("Adding an author to the database.");
    }

}
