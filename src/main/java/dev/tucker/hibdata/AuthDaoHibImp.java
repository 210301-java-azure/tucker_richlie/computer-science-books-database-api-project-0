package dev.tucker.hibdata;

import dev.tucker.data.AuthDAO;
import dev.tucker.models.Admin;
import dev.tucker.models.Member;
import dev.tucker.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AuthDaoHibImp implements AuthDAO {

    Logger logger = LoggerFactory.getLogger(AuthDaoHibImp.class);

    @Override
    public int checkAdminCredentials(String userName, String password) {

        try (Session s = HibernateUtil.getSession()) {
            Query<Admin> q = s.createQuery("from Admin where (userName = :userName) and (passWord = :password)", Admin.class);
            q.setParameter("userName", userName);
            q.setParameter("password", password);
            List<Admin> adminList = q.list();
            if (!adminList.isEmpty()) {
                return 1;
            }
            return 0;

        }
    }


    @Override
    public int checkUserCredentials(String email, String password) {

        try (Session s = HibernateUtil.getSession()) {
            Query<Member> q = s.createQuery("from Member where (email = :email) and (password = :password)", Member.class);
            q.setParameter("email", email);
            q.setParameter("password", password);
            List<Member> memberList = q.list();
            if (!memberList.isEmpty()) {
                return 1;
            }
            return 0;

        }

    }

    @Override
    public void addNewUser(Member user) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.persist(user);
            tx.commit();
        }
    }
}
