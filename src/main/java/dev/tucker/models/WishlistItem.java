package dev.tucker.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class WishlistItem implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name="title")
    private Book bookTitle;
    private boolean hasRead;


    @Id
    @ManyToOne
    @JoinColumn(name="email")
    private Member user;


    public WishlistItem() {
        super();
    }

    public WishlistItem(Book bookTitle, boolean hasRead) {
        this.bookTitle = bookTitle;
        this.hasRead = hasRead;
    }

    public WishlistItem(Book bookTitle, boolean hasRead, Member user) {
        this.bookTitle = bookTitle;
        this.hasRead = hasRead;
        this.user = user;
    }

    public Book getBook() {
        return bookTitle;
    }

    public void setBook(Book bookTitle) {
        this.bookTitle = bookTitle;
    }

    public boolean isHasRead() {
        return hasRead;
    }

    public void setHasRead(boolean hasRead) {
        this.hasRead = hasRead;
    }


    public Member getUser() {
        return user;
    }

    public void setUser(Member user) {
        this.user = user;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WishlistItem that = (WishlistItem) o;
        return hasRead == that.hasRead && Objects.equals(bookTitle, that.bookTitle) && Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookTitle, hasRead, user);
    }

    @Override
    public String toString() {
        return "WishlistItem{" +
                "bookTitle=" + bookTitle +
                ", hasRead=" + hasRead +
                ", user=" + user +
                '}';
    }
}
